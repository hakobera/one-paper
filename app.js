/**
 * Module dependencies.
 */

var express = require('express')
  , everyauth = require('everyauth')
  , url = require('url')
  , config = require('./lib/config')
  , User = require('./lib/user');

var app = module.exports = express.createServer();

var env = process.env;
console.log(env);

everyauth.twitter
  .consumerKey(env.TWITTER_CONSUMER_KEY)
  .consumerSecret(env.TWITTER_CONSUMER_SECRET)
  .findOrCreateUser(function(session, accessToken, accessTokenSecret, twitterUserMetadata) {
    console.log(twitterUserMetadata);
    var user = new User();
    user.metadata = twitterUserMetadata;
    return user;
  })
  .redirectPath('/');

// Configuration

app.configure(function(){
  app.set('views', __dirname + '/views');
  app.set('view engine', 'ejs');
  app.set('view options', {
    layout: false
  });
  app.use(express.bodyParser());
  app.use(express.cookieParser());
  app.use(express.methodOverride());
  app.use(express.static(__dirname + '/public'));
});

app.configure('development', function() {
  app.use(express.session({ secret: env.SESSION_SECRET }));
  app.use(everyauth.middleware());
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});

app.configure('production', function() {
  var RedisStore = require('connect-redis')(express);
  var redisUri = url.parse(env.REDISTOGO_URL);
  var store = new RedisStore({
    host: redisUri.hostname,
    port: redisUri.port,
    pass: redisUri.auth.split(':')[1]
  });
  app.use(express.session({ secret: env.SESSION_SECRET, store: store }));
  app.use(everyauth.middleware());
  app.use(express.errorHandler());
});

// Routes

app.get('/', function(req, res){
  res.render('index', {
    title: 'One Paper'
  });
});

app.get('/logout', function(req, res) {
  req.logout();
});

everyauth.helpExpress(app);

var port = env.PORT || config.development.port;
app.listen(port);
console.log("Express server listening on port %d in %s mode", app.address().port, app.settings.env);
