var httpProxy = require('http-proxy')
  , config = require('./lib/config');

var options = {
  hostnameOnly: true,
  router: {
    'one-paper.herokuapp.com': '127.0.0.1:' + config.development.port
  }
}

var port = 80;
var proxyServer = httpProxy.createServer(options).listen(port);

console.log('httpProxy listening on port %d.', port);
console.log(options);